build-properties:
	echo "=== generating build properties file ==="
	echo "GIT_COMMIT=${GIT_COMMIT}" > build.properties
	echo "GIT_TAG=${GIT_TAG}" >> build.properties
	echo "PROJECT_NAME=${PROJECT_NAME}" >> build.properties
	echo "DOCKER_IMAGE=${DOCKER_IMAGE}" >> build.properties
	echo "VERSION=${VERSION}" >> build.properties

deps:
	go install github.com/golang/mock/mockgen@latest

gen:
	@echo "=== Code Generation ==="
	@go generate ./...


test: mocks
	@echo "Running tests"
	go test -coverprofile=.coverage.out -p 1 -timeout 120s ./...
	go tool cover -func=.coverage.out | tail -1 

clean-mocks:
	find . -name "mock_*" -type f -exec rm -rf {} +

mocks: clean-mocks gen

build: deps gen build-properties
	CGO_ENABLED=0 GOOS=linux GO_ARCH=amd64 go build -a -installsuffix cgo -o dist/zcounter cmd/main.go

run:
	./build-and-run.sh

gomod:
	go mod download
	go mod tidy
	go mod vendor
