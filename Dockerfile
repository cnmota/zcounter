FROM golang:1.17-alpine AS builder

RUN apk add git make

COPY . /build/
WORKDIR /build/

RUN make build

FROM scratch

COPY --from=builder /etc/ssl/certs/ /etc/ssl/certs/
COPY --from=builder /etc/nsswitch.conf /etc/
COPY --from=builder /build/dist/ /bin/
