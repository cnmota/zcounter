package metrics

import (
	"context"
	"os"

	"github.com/rs/zerolog"
)

type LogBasedMetter struct {
	sys string
	out zerolog.Logger
}

func NewLogBasedMetter(sys string) LogBasedMetter {
	return LogBasedMetter{
		sys: sys,
		out: zerolog.New(os.Stdout),
	}
}

func labelsAsDic(labels map[string]interface{}) *zerolog.Event {
	e := zerolog.Dict()

	for k, v := range labels {
		e.Interface(k, v)
	}

	return e
}

func (m LogBasedMetter) RegisterOnHistogram(ctx context.Context, name string, val int64, labels map[string]interface{}) error {
	m.out.Info().Str("system", m.sys).Str("metric", name).Dict("labels", labelsAsDic(labels)).Int64("value", val).Msg("")

	return nil
}

func (m LogBasedMetter) IncrementCounter(ctx context.Context, name string, val int64, labels map[string]interface{}) error {
	m.out.Info().Str("system", m.sys).Str("metric", name).Dict("labels", labelsAsDic(labels)).Int64("value", val).Msg("")

	return nil
}
