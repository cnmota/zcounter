package storage

import (
	"fmt"
	"hash/fnv"
	"sync"
)

// Set this type holds records allowing for item partitioning to improve concurrency
type Set struct {
	siz        int8
	muxes      []sync.RWMutex
	partitions []map[string]struct{}
}

func NewSet(siz int8) (*Set, error) {
	if siz <= 0 {
		return nil, fmt.Errorf("invalid part size should be greater than 0")
	}

	return &Set{
		siz:        siz,
		muxes:      make([]sync.RWMutex, siz),
		partitions: make([]map[string]struct{}, siz),
	}, nil
}

func (s *Set) Add(elem string) {
	idx := s.shardFor(elem)

	s.muxes[idx].Lock()
	defer s.muxes[idx].Unlock()

	partition := s.partitions[idx]
	if len(partition) == 0 {
		partition = make(map[string]struct{})
	}

	partition[elem] = struct{}{}
	s.partitions[idx] = partition
}

func (s *Set) Has(elem string) bool {
	idx := s.shardFor(elem)

	s.muxes[idx].RLock()
	defer s.muxes[idx].RUnlock()

	partition := s.partitions[idx]
	_, ok := partition[elem]

	return ok
}

func (s *Set) Cardinality() int64 {
	var cnt = 0

	for i := range s.partitions {
		s.muxes[i].RLock()
		cnt += len(s.partitions[i])
		s.muxes[i].RUnlock()
	}

	return int64(cnt)
}

func (s *Set) shardFor(elem string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(elem))
	return h.Sum32() % uint32(s.siz)
}
