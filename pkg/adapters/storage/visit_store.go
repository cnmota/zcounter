package storage

import (
	"context"
	"sync"
)

type VisitStore struct {
	ssSize int8 // partitions per set
	mux    sync.RWMutex
	keys   map[string]*Set
}

type VisitsStoreOption func(*VisitStore) error

func WithShardedSetSize(siz int8) VisitsStoreOption {
	return func(s *VisitStore) error {
		s.ssSize = siz

		return nil
	}
}

func NewVisitStore(opts ...VisitsStoreOption) (*VisitStore, error) {
	s := VisitStore{
		ssSize: 1,
		keys:   make(map[string]*Set),
	}

	for _, opt := range opts {
		opt(&s)
	}

	return &s, nil
}

func (r *VisitStore) AddElementToKey(ctx context.Context, key, elem string) (bool, error) {
	set, err := r.setForKey(key)
	if err != nil {
		return false, err
	}

	if set.Has(elem) {
		return false, nil
	}

	set.Add(elem)

	return true, nil
}

func (r *VisitStore) CardinalityFor(ctx context.Context, key string) (int64, error) {
	set, ok := r.getSetFor(key)
	if !ok {
		return 0, nil
	}

	return set.Cardinality(), nil
}

func (r *VisitStore) setForKey(key string) (*Set, error) {
	var err error

	set, ok := r.getSetFor(key)
	if !ok {
		set, err = r.addSetFor(key)
		if err != nil {
			return nil, err
		}
	}

	return set, nil
}

func (r *VisitStore) getSetFor(key string) (*Set, bool) {
	r.mux.RLock()
	defer r.mux.RUnlock()

	s, ok := r.keys[key]

	return s, ok
}

func (r *VisitStore) addSetFor(key string) (*Set, error) {
	r.mux.Lock()
	defer r.mux.Unlock()

	s, ok := r.keys[key]
	if ok {
		return s, nil
	}

	set, err := NewSet(r.ssSize)
	if err != nil {
		return nil, err
	}

	r.keys[key] = set

	return r.keys[key], nil
}
