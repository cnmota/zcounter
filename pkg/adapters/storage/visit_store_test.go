package storage

import (
	"context"
	"fmt"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

type tuple struct {
	key, elem string
}

func TestVisitStore(t *testing.T) {
	var ctx = context.Background()

	for i := 1; i <= 4; i++ {
		tcs := []struct {
			description   string
			err           assert.ErrorAssertionFunc
			inputs        []tuple
			cardinalities map[string]int64
		}{
			{
				description: "simple duplicate entries",
				inputs: []tuple{
					{key: "s01", elem: "12910920"},
					{key: "s01", elem: "12910920"},
					{key: "s01", elem: "12910921"},
					{key: "s01", elem: "12910922"},
					{key: "s01", elem: "12910923"},
					{key: "s01", elem: "12910924"},
					{key: "s01", elem: "12910925"},
					{key: "s01", elem: "12910926"},
					{key: "s01", elem: "12910927"},
					{key: "s01", elem: "12910928"},
					{key: "s01", elem: "12910929"},
					{key: "s01", elem: "12910921"},
					{key: "s02", elem: "12910929"},
					{key: "s02", elem: "12910927"},
					{key: "s02", elem: "12910929"},
					{key: "s03", elem: "12910926"},
					{key: "s03", elem: "12910927"},
					{key: "s03", elem: "12910928"},
					{key: "s02", elem: "12910927"},
					{key: "s02", elem: "12910929"},
					{key: "s02", elem: "12910927"},
				},
				cardinalities: map[string]int64{
					"s01": 10,
					"s02": 2,
					"s03": 3,
				},
			},
		}

		for _, tc := range tcs {
			t.Run(fmt.Sprintf("%s, partitions:%d", tc.description, i), func(t *testing.T) {
				store, err := NewVisitStore(WithShardedSetSize(int8(i)))
				if i == 0 {
					assert.Error(t, err, "should have returned the expected error")
					return
				}

				assert.NoError(t, err, "should not have returned an error")

				wg := sync.WaitGroup{}
				for _, inc := range tc.inputs {
					inc := inc
					wg.Add(1)
					go func() {
						if _, err := store.AddElementToKey(ctx, inc.key, inc.elem); err != nil {
							t.Error(err)
						}

						wg.Done()
					}()
				}
				wg.Wait()

				for k, v := range tc.cardinalities {
					actual, _ := store.CardinalityFor(ctx, k)

					assert.Equal(t, v, actual, fmt.Sprintf("should have returned expected cardinality for %s", k))
				}
			})
		}
	}
}
