package handlers

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
)

func TestHandleAddVisit(t *testing.T) {
	var ctrl = gomock.NewController(t)
	defer ctrl.Finish()

	tcs := []struct {
		description  string
		reqBody      []byte
		service      func() *MockCounterService
		expectedCode int
		expectedBody []byte
	}{
		{
			description:  "when body is missing",
			service:      func() *MockCounterService { return NewMockCounterService(ctrl) },
			expectedCode: 400,
			expectedBody: []byte(`{`),
		},
		{
			description:  "when body is invalid",
			service:      func() *MockCounterService { return NewMockCounterService(ctrl) },
			reqBody:      []byte(`{"id":12,a}`),
			expectedCode: 400,
			expectedBody: []byte(``),
		},
		{
			description:  "when body is valid, missing url",
			service:      func() *MockCounterService { return NewMockCounterService(ctrl) },
			reqBody:      []byte(`{"user":"me"}`),
			expectedCode: 400,
			expectedBody: []byte(`{"msg":"invalid url"}`),
		},
		{
			description:  "when body is valid, missing user",
			service:      func() *MockCounterService { return NewMockCounterService(ctrl) },
			reqBody:      []byte(`{"url":"/ze"}`),
			expectedCode: 400,
			expectedBody: []byte(`{"msg":"invalid user"}`),
		},
		{
			description: "when body is valid, service fails",
			service: func() *MockCounterService {
				m := NewMockCounterService(ctrl)

				m.EXPECT().RegisterVisit(gomock.Any(), "/home", "mota").Return(errors.New("boom"))

				return m
			},
			reqBody:      []byte(`{"url":"/home", "user": "mota"}`),
			expectedCode: 500,
			expectedBody: []byte(`{"error":"boom"}`),
		},
		{
			description: "happy path",
			service: func() *MockCounterService {
				m := NewMockCounterService(ctrl)

				m.EXPECT().RegisterVisit(gomock.Any(), "/home", "mota").Return(nil)

				return m
			},
			reqBody:      []byte(`{"url":"/home", "user": "mota"}`),
			expectedCode: 200,
			expectedBody: []byte(``),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodPost, "/", bytes.NewReader(tc.reqBody))
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

			rec := httptest.NewRecorder()
			ctx := echo.New().NewContext(req, rec)

			h := NewCounterHandler(tc.service(), zerolog.New(os.Stdout))
			h.AddVisit(ctx)

			resB, err := ioutil.ReadAll(rec.Result().Body)
			if err != nil {
				t.Error(err)
			}

			assert.Equal(t, tc.expectedCode, rec.Result().StatusCode, "should have returned the expected status code")
			assert.Contains(t, string(resB), string(tc.expectedBody), "should have returned the expected status code")
		})
	}
}

func TestVisitsFor(t *testing.T) {
	var ctrl = gomock.NewController(t)
	defer ctrl.Finish()

	tcs := []struct {
		description  string
		url          string
		service      func() *MockCounterService
		expectedCode int
		expectedBody []byte
	}{
		{
			description:  "when no url is provided",
			service:      func() *MockCounterService { return NewMockCounterService(ctrl) },
			expectedCode: 400,
			expectedBody: []byte(`{`),
		},
		{
			description: "when url is valid, service fails",
			url:         "/zURL",
			service: func() *MockCounterService {
				m := NewMockCounterService(ctrl)

				m.EXPECT().UniqueVisitors(gomock.Any(), "/zURL").Return(int64(0), errors.New("boom"))

				return m
			},
			expectedCode: 500,
			expectedBody: []byte(`{"error":"boom"}`),
		},
		{
			description: "happy path count == 0",
			url:         "/zURL",
			service: func() *MockCounterService {
				m := NewMockCounterService(ctrl)

				m.EXPECT().UniqueVisitors(gomock.Any(), "/zURL").Return(int64(0), nil)

				return m
			},
			expectedCode: 200,
			expectedBody: []byte(`{"url":"/zURL","count":0}`),
		},
		{
			description: "happy path count != 0",
			url:         "/zURL",
			service: func() *MockCounterService {
				m := NewMockCounterService(ctrl)

				m.EXPECT().UniqueVisitors(gomock.Any(), "/zURL").Return(int64(120), nil)

				return m
			},
			expectedCode: 200,
			expectedBody: []byte(`{"url":"/zURL","count":120}`),
		},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/?url=%s", tc.url), nil)
			req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

			rec := httptest.NewRecorder()
			ctx := echo.New().NewContext(req, rec)

			h := NewCounterHandler(tc.service(), zerolog.New(os.Stdout))
			h.VisitorsFor(ctx)

			resB, err := ioutil.ReadAll(rec.Result().Body)
			if err != nil {
				t.Error(err)
			}

			assert.Equal(t, tc.expectedCode, rec.Result().StatusCode, "should have returned the expected status code")
			assert.Contains(t, string(resB), string(tc.expectedBody), "should have returned the expected status code")
		})
	}
}
