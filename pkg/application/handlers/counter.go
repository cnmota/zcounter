package handlers

import (
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
)

//go:generate mockgen -source=counter.go -destination=mock_counter.go -package=handlers

type CounterService interface {
	RegisterVisit(ctx context.Context, url, user string) error
	UniqueVisitors(ctx context.Context, url string) (int64, error)
}

type CounterHandler struct {
	cntSvc CounterService
}

func NewCounterHandler(svc CounterService, log zerolog.Logger) CounterHandler {
	return CounterHandler{cntSvc: svc}
}

type addVisitRequest struct {
	Url  string `json:"url"`
	User string `json:"user"`
}

type urlCountResponse struct {
	Url string `json:"url"`
	Cnt int64  `json:"count"`
}

func (h CounterHandler) AddVisit(ctx echo.Context) error {
	var req addVisitRequest

	if err := ctx.Bind(&req); err != nil {
		return ctx.NoContent(http.StatusBadRequest)
	}

	if req.User == "" {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"msg": "invalid user",
		})
	}

	if req.Url == "" {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"msg": "invalid url",
		})
	}

	if err := h.cntSvc.RegisterVisit(ctx.Request().Context(), req.Url, req.User); err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"error": err.Error(),
		})
	}

	return ctx.NoContent(http.StatusOK)
}

func (h CounterHandler) VisitorsFor(ctx echo.Context) error {
	var url = ctx.QueryParam("url")

	if url == "" {
		return ctx.JSON(http.StatusBadRequest, map[string]interface{}{
			"msg": "invalid url",
		})
	}

	val, err := h.cntSvc.UniqueVisitors(ctx.Request().Context(), url)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, map[string]interface{}{
			"error": err.Error(),
		})
	}

	return ctx.JSON(http.StatusOK, urlCountResponse{Url: url, Cnt: val})
}
