package services

import (
	"context"
	"fmt"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestUniqueVisitors(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)
	defer ctrl.Finish()

	tcs := []struct {
		description string
		store       func() *MockVisitsStore
		metrics     func() *MockMetricStore
		expected    int64
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when store call fails",
			err:         assert.Error,
			expected:    0,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().CardinalityFor(gomock.Any(), "someKey").Return(int64(0), fmt.Errorf("boom"))

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     true,
				}).Return(nil)

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     true,
				}).Return(nil)

				return m
			},
		},
		{
			description: "when store call ok, metrics fail",
			err:         assert.NoError,
			expected:    10,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().CardinalityFor(gomock.Any(), "someKey").Return(int64(10), nil)

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     false,
				}).Return(fmt.Errorf("boom1"))

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     false,
				}).Return(fmt.Errorf("boom1"))

				return m
			},
		},
		{
			description: "happy path",
			err:         assert.NoError,
			expected:    10,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().CardinalityFor(gomock.Any(), "someKey").Return(int64(10), nil)

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     false,
				}).Return(nil)

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "count",
					"error":     false,
				}).Return(nil)

				return m
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			svc := NewUrlCounter(tc.store(), tc.metrics())
			cnt, err := svc.UniqueVisitors(ctx, "someKey")

			tc.err(t, err, "should have returned expected error assertion")
			assert.Equal(t, tc.expected, cnt, "should have returned expected result")
		})
	}
}

func TestRegisterVisit(t *testing.T) {
	var (
		ctx  = context.Background()
		ctrl = gomock.NewController(t)
	)

	tcs := []struct {
		description string
		store       func() *MockVisitsStore
		metrics     func() *MockMetricStore
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when store call fails",
			err:         assert.Error,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().AddElementToKey(gomock.Any(), "someKey", "someUser").Return(false, fmt.Errorf("boom"))

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     false,
					"error":     true,
				}).Return(nil)

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     false,
					"error":     true,
				}).Return(nil)

				return m
			},
		},
		{
			description: "when store succeeds but nothing was added",
			err:         assert.NoError,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().AddElementToKey(gomock.Any(), "someKey", "someUser").Return(false, nil)

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     false,
					"error":     false,
				}).Return(nil)

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     false,
					"error":     false,
				}).Return(nil)

				return m
			},
		},
		{
			description: "when store succeeds visitor was added",
			err:         assert.NoError,
			store: func() *MockVisitsStore {
				m := NewMockVisitsStore(ctrl)

				m.EXPECT().AddElementToKey(gomock.Any(), "someKey", "someUser").Return(true, nil)

				return m
			},
			metrics: func() *MockMetricStore {
				m := NewMockMetricStore(ctrl)

				m.EXPECT().RegisterOnHistogram(gomock.Any(), visitHistogramMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     true,
					"error":     false,
				}).Return(nil)

				m.EXPECT().IncrementCounter(gomock.Any(), visitCountMetric, gomock.Any(), map[string]interface{}{
					"operation": "register",
					"added":     true,
					"error":     false,
				}).Return(nil)

				return m
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			svc := NewUrlCounter(tc.store(), tc.metrics())
			err := svc.RegisterVisit(ctx, "someKey", "someUser")

			tc.err(t, err, "should have returned expected error assertion")
		})
	}
}
