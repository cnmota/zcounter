package services

import (
	"context"
	"time"
)

//go:generate mockgen -source=counter.go -destination=mock_counter.go -package=services

const (
	visitHistogramMetric = "visit_histogram"
	visitCountMetric     = "visit_counter"
)

type VisitsStore interface {
	AddElementToKey(ctx context.Context, key, elem string) (bool, error)
	CardinalityFor(ctx context.Context, key string) (int64, error)
}

type MetricStore interface {
	RegisterOnHistogram(ctx context.Context, name string, val int64, labels map[string]interface{}) error
	IncrementCounter(ctx context.Context, name string, val int64, labels map[string]interface{}) error
}

type UrlCounter struct {
	store   VisitsStore
	metrics MetricStore
}

func NewUrlCounter(s VisitsStore, m MetricStore) UrlCounter {
	return UrlCounter{store: s, metrics: m}
}

func (c UrlCounter) RegisterVisit(ctx context.Context, url, user string) error {
	var (
		start = time.Now()
		added bool
		err   error
	)

	defer func(bool) {
		c.metrics.RegisterOnHistogram(ctx, visitHistogramMetric, time.Since(start).Nanoseconds(), map[string]interface{}{
			"operation": "register",
			"added":     added,
			"error":     err != nil,
		})

		c.metrics.IncrementCounter(ctx, visitCountMetric, 1, map[string]interface{}{
			"operation": "register",
			"added":     added,
			"error":     err != nil,
		})
	}(added)

	added, err = c.store.AddElementToKey(ctx, url, user)
	if err != nil {
		return err
	}

	return nil
}

func (c UrlCounter) UniqueVisitors(ctx context.Context, url string) (int64, error) {
	var (
		start = time.Now()
		res   int64
		err   error
	)

	defer func() {
		c.metrics.RegisterOnHistogram(ctx, visitHistogramMetric, time.Since(start).Nanoseconds(), map[string]interface{}{
			"operation": "count",
			"error":     err != nil,
		})

		c.metrics.IncrementCounter(ctx, visitCountMetric, 1, map[string]interface{}{
			"operation": "count",
			"error":     err != nil,
		})
	}()

	res, err = c.store.CardinalityFor(ctx, url)

	return res, err
}
