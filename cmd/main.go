package main

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/cnmota/zcounter/cmd/daemons"
)

func main() {
	rootCmd := &cobra.Command{Use: "counter [SERVICE]"}

	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	rootCmd.AddCommand(commandFor("api", logger))

	if err := rootCmd.Execute(); err != nil {
		logger.Error().Err(err).Msg("failed to execute command")
	}
}

func commandFor(appName string, logger zerolog.Logger) *cobra.Command {
	return &cobra.Command{
		Use:   appName,
		Short: fmt.Sprintf("starts %s", appName),
		RunE: func(cmd *cobra.Command, args []string) error {
			daemons.NewAPI(
				daemons.WithLogger(logger),
				daemons.WithPort(8080),
			).Start()

			return nil
		},
	}
}
