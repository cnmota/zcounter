package daemons

import (
	"fmt"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog"
	"github.com/ziflex/lecho/v3"
	"gitlab.com/cnmota/zcounter/pkg/adapters/metrics"
	"gitlab.com/cnmota/zcounter/pkg/adapters/storage"
	"gitlab.com/cnmota/zcounter/pkg/application/handlers"
	"gitlab.com/cnmota/zcounter/pkg/application/services"
)

type API struct {
	port   int
	logger zerolog.Logger
}

type APIOption func(a *API)

func WithLogger(l zerolog.Logger) APIOption {
	return func(a *API) {
		a.logger = l
	}
}

func WithPort(p int) APIOption {
	return func(a *API) {
		a.port = p
	}
}

func NewAPI(opts ...APIOption) API {
	a := API{
		port:   10080,
		logger: zerolog.New(os.Stdout),
	}

	for _, opt := range opts {
		opt(&a)
	}

	return a
}

func (a API) Start() error {
	e := echo.New()

	if err := a.bootstrap(e); err != nil {
		return err
	}

	e.HideBanner = true
	e.HidePort = true
	e.Logger = lecho.From(a.logger)

	e.Logger.Info(fmt.Sprintf("started service on port:%d", a.port))
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", a.port)))

	return nil
}

func (a API) bootstrap(e *echo.Echo) error {
	h, err := a.counterHandler()
	if err != nil {
		return err
	}

	e.POST("/api/1/visits", h.AddVisit)
	e.GET("/api/1/visits", h.VisitorsFor)

	return nil
}

func (a API) counterHandler() (handlers.CounterHandler, error) {
	dat, err := storage.NewVisitStore(storage.WithShardedSetSize(8))
	if err != nil {
		return handlers.CounterHandler{}, err
	}

	svc := services.NewUrlCounter(dat, metrics.NewLogBasedMetter("counter"))

	return handlers.NewCounterHandler(svc, a.logger), nil
}
