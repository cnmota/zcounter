# zcounter

zCounter is a simple API Service providing an URL / User counter, internally it uses an in-memory storage, based on keys containing a set of distinct users.

The algorithm is quite simple, whenever a new event is received the system:

* Fetches the set for key == url
* Adds the User to the Set, since we are storing values on a Set no duplicates are allowed.

All operations are thread / go-routine safe, and to improve concurrency for busy and high volume URL's Sets can be Partitioned this changes the previous workflow slightly, so when using Partitioned Sets ( default is 8) the workflow now has an additional step:

* Fetches the set for URL and then the Partition Subset that matches HashFunc(user) mod Number of Partitions.

Although this step adds complexity it allows for better concurrency and less lock contention when we have multiple events for distinct users registering visits on popular URLs.

## Improvements

Depending on Data Distribution after Metric Analysis we could convert the maps used to store distinct users to a probabilistic counter like an HyperLogLog structure allowing us to save a huge amount of memory and allocations with minimum error.

Multiple improvements could be made, like micro batching events or even using kafka to perform lockless operations, but those are beside the scope and would require data behaviour and load analysis.
## Running the application

### Compiling via comand line

```
make run
```

### Building and running the binary

**NOTE:** building for amd64 arch

```
make build
dist/zcounter
```

### Building and running docker image

```
./build-and-run.sh
```

or 

```
make run
```

this will start the API on port 8080

## API 

The API provides two endpoints:

* `/api/1/visits`

The endpoint provides two methods:

| Method | Action | Headers | Body | Query Params | Possible Responses |
| ------ | ------- | ---- | --------- | ----- | ----- | 
| POST   | Registers a URL, User Visit | Content-type: application/json | `{"url":"/home", "user": "nuno"}` | none | 200, 400, 500 |
| GET   | Gets Count of Distinct users visiting the supplied URL | Content-type: application/json | none | url=/zeeurl | 200, 400, 500 |

### Examples
#### Registering a user visit on a URL

```
curl -X POST "http://localhost:8080/api/1/visits" -H "Content-type: application/json" -d '{"url":"/home", "user": "nuno"}'
curl -X POST "http://localhost:8080/api/1/visits" -H "Content-type: application/json" -d '{"url":"/home", "user": "mota"}'
curl -X POST "http://localhost:8080/api/1/visits" -H "Content-type: application/json" -d '{"url":"/cart", "user": "nuno"}'
```

#### Obtaining distinct visits to a specific URL

```
curl "http://localhost:8080/api/1/visits?url=/home"
```

This will return:

```
{"url":"/home","count":2}
```
